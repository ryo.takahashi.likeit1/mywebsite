CREATE TABLE bbs(
	id SERIAL PRIMARY KEY,
	title varchar(255) NOT NULL,
	user_id varchar(255) NOT NULL,
	category_id int NOT NULL,
	text varchar(2000) NOT NULL,
	create_date DATETIME NOT NULL
	);
