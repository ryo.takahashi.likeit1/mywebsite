CREATE TABLE report(
	id SERIAL PRIMARY KEY,
	bbs_id int NOT NULL,
	bbs_title varchar(255) NOT NULL,
	comment_id int UNIQUE NOT NULL,
	comment varchar(2000) NOT NULL,
	report_date DATETIME NOT NULL
	);
