CREATE TABLE user(
	id SERIAL PRIMARY KEY,
	login_id varchar(255) UNIQUE NOT NULL,
	name varchar(255) NOT NULL,
	password varchar(255) NOT NULL,
	birth_date date NOT NULL,
	create_date DATETIME NOT NULL,
	update_date DATETIME NOT NULL
	);
