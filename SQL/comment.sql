CREATE TABLE comment(
	id SERIAL PRIMARY KEY,
	bbs_id int NOT NULL,
	user_id varchar(255) NOT NULL,
	user_name varchar(255) NOT NULL,
	text varchar(2000) NOT NULL,
	create_date DATETIME NOT NULL
	);
