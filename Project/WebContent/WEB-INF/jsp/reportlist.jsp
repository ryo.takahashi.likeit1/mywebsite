<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">

    <title></title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style1.css">

</head>

<body>
    <div class="container header">
        <div class="row">
            <h1 class="col-3" style="margin-left: 20px">
                <a class="home" href="/MyWebSite/Login" style="text-decoration: none">MyWebSite</a>
            </h1>
            <div class="col" style="align-self: flex-end; text-align: right">
                <span>${userInfo.name }さん</span>
                <a class="mypage" href="/MyWebSite/UserDetail">マイページ</a>
                <a class="logout" href="/MyWebSite/Logout">ログアウト</a>
            </div>
        </div>
    </div>

    <div class="container body col-7" style="padding-top: 30px;padding-bottom: 30px;">


        <div align=center style="padding-top: 20px">


        </div>

        <div align="center">
            <h5>報告コメント一覧</h5>
        </div>

        <div style="padding-top: 20px;">
            <ol>
           		<div style="margin-bottom: 5px">掲示板タイトル</div>
            	<c:forEach var="report" items="${reportList }">
	                <li><a href="/MyWebSite/ReportDetail?id=${report.id }">${report.bbsTitle }</a></li>
	            </c:forEach>
            </ol>
        </div>

    </div>

</body>

</html>