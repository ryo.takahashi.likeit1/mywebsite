<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">

    <title></title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style1.css">

</head>

<body>
    <div class="container header">
        <div class="row">
            <h1 class="col-3" style="margin-left: 20px">
                <a class="home" href="" style="text-decoration: none">MyWebSite</a>
            </h1>

        </div>
    </div>

    <div align="center" class="col-6 container body" style="padding-top: 30px;padding-bottom: 30px;">
        <dif style="text-align: center;">
            <h4 style="margin: 30px">ユーザーを登録しました</h4>
        </dif>

        <dif>
            <a href="/MyWebSite/Login" style="text-decoration: underline">ログイン画面へ戻る</a>
        </dif>
    </div>

</body>

</html>