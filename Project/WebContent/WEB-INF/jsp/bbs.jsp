<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">

    <title></title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style1.css">

</head>

<body>
    <div class="container header">
        <div class="row">
            <h1 class="col-3" style="margin-left: 20px">
                <a class="home" href="/MyWebSite/Login" style="text-decoration: none">MyWebSite</a>
            </h1>
            <div class="col" style="align-self: flex-end; text-align: right">
                <span>${userInfo.name }さん</span>
                <a class="mypage" href="/MyWebSite/UserDetail">マイページ</a>
                <a class="logout" href="/MyWebSite/Logout">ログアウト</a>
            </div>
        </div>
    </div>

    <div class="container body" style="padding-top: 30px;padding-bottom: 30px; margin-bottom: 30px">
        <h4><span class="bbstitle">${bbs.title }</span></h4>

        <div style="padding-top: 10px">
            カテゴリー：
            <a class="name" href="">${ct.name }</a>
        </div>

		<div style="margin-left: 15px; margin-top: 5px">
            <span style="color: red">${errMsg }</span>
        </div>

        <div class="container" style="padding-top: 10px;">
            <form action="/MyWebSite/BBS" method="post">
            	<input type="hidden" name="bbsId" value="${bbs.id }">
            	<input type="hidden" name="bbs" value="${bbs }">
            	<input type="hidden" name="ct" value="${ct }">
            	<input type="hidden" name="user" value="${user }">
                <textarea name="text" placeholder="本文" rows="5" cols="60"></textarea>
                <br><input type="submit" value="書き込む">
            </form>
        </div>

        <div >
            <ol>
            <div>
                <br><li>名前：
                <a class="name" href="/MyWebSite/UserListDetail?id=${user.id }">${user.name }</a>
                <br>${bbs.createDate }
                <div class="text" >${bbs.text }</div>
                </li>
            </div>
            <div>
            	<c:forEach var="cm" items="${cmList }">
          	      <br><li>名前：
            	    <a class="name" href="/MyWebSite/UserListDetail?id=${cm.userId }">${cm.userName }</a>
                	<br>${cm.createDate }
                	<c:if test="${cm.text != '<br><span style=\"color: red\">※このコメントは削除されました</span>'}">
                		<a href="/MyWebSite/Report?id=${cm.id }" style="margin-left: 30px">このコメントを報告する</a>
                	</c:if>
                	<div class="text">${cm.text }</div>
                	</li>
                </c:forEach>
            </div>
            </ol>
        </div>

        <div class="container" style="padding-top: 30px">
            <a href="/MyWebSite/Home" style="text-decoration: underline; color: blue">ホーム</a>
            <a href="/MyWebSite/BBSList" style="text-decoration: underline; color: blue; margin-left: 30px">掲示板一覧</a>
        </div>
    </div>

</body>

</html>