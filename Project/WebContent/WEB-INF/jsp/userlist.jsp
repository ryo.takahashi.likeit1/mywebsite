<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">

    <title></title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style1.css">

</head>

<body>
    <div class="container header">
        <div class="row">
            <h1 class="col-3" style="margin-left: 20px">
                <a class="home" href="/MyWebSite/Login" style="text-decoration: none">MyWebSite</a>
            </h1>

            <div class="col" style="align-self: flex-end; text-align: right">
                <span>${userInfo.name }さん</span>
                <a class="mypage" href="/MyWebSite/UserDetail">マイページ</a>
                <a class="logout" href="/MyWebSite/Logout">ログアウト</a>
            </div>

        </div>
    </div>

    <div class="container body" style="padding-top: 30px;padding-bottom: 30px; width: 500px">
        <h2 style="text-align: center">ユーザー一覧</h2>

        <div align=center>
            <form action="/MyWebSite/UserList" method="post">
                <input name="name" type="text" placeholder="名前">
                <input type="submit" value="検索">
            </form>
        </div>

        <div align="center" style="padding-top: 15px">
            <table>
                <tr>
                    <td class="detail" style="background-color:gainsboro">ID</td>
                    <td class="detail" style="background-color: gainsboro">名前</td>
                </tr>
                <!-- 登録されているユーザーの数毎に列を追加 -->
                <c:forEach var="user" items="${userlist }">
                	<tr>
                    	<td class="detail"><a href="/MyWebSite/UserListDetail?id=${user.id}">${user.loginId }</a></td>
                    	<td class="detail">${user.name }</td>
                	</tr>
                </c:forEach>
            </table>
        </div>

        <a href="javascript:history.back()" style="text-decoration: underline">戻る</a>

    </div>

</body>

</html>