<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">

    <title></title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style1.css">

</head>

<body>
    <div class="container header">
        <div class="row">
            <h1 class="col-3" style="margin-left: 20px">
                <a class="home" href="/MyWebSite/Login" style="text-decoration: none">MyWebSite</a>
            </h1>

            <div class="col" style="align-self: flex-end; text-align: right">
                <span>${userInfo.name }さん</span>
                <a class="mypage" href="/MyWebSite/UserDetail">マイページ</a>
                <a class="logout" href="/MyWebSite/Logout">ログアウト</a>
            </div>
        </div>
    </div>

    <div class="container body col-5" style="padding-top: 50px;padding-bottom: 50px;">
        <div style="text-align: center">
            <div>ユーザー：${user.loginId } を削除します
            </div>

            <table align=center style="margin-top: 20px">
                <tr>
                    <td>
                        <form action="/MyWebSite/UserDetail">
                            <input type="submit" value="キャンセル" style="width: 100px;margin-right: 30px">
                        </form>
                    </td>
                    <td>
                        <form action="/MyWebSite/UserDelete" method="post">
                        	<input type="hidden" name="id" value="${user.id}">
                            <input type="submit" value="削除" style="width: 100px">
                        </form>
                    </td>
                </tr>
            </table>
        </div>

        <a href="javascript:history.back()" style="text-decoration: underline">戻る</a>

    </div>

</body>

</html>