<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">

    <title>ユーザー編集</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style1.css">

</head>

<body>
    <div class="container header">
        <div class="row">
            <h1 class="col-3" style="margin-left: 20px">
                <a class="home" href="/MyWebSite/Login" style="text-decoration: none">MyWebSite</a>
            </h1>

            <div class="col" style="align-self: flex-end; text-align: right">
                <span>${userInfo.name }さん</span>
                <a class="mypage" href="/MyWebSite/UserDetail">マイページ</a>
                <a class="logout" href="/MyWebSite/Logout">ログアウト</a>
            </div>

        </div>
    </div>

    <div class="container body col-5" style="padding-top: 30px;padding-bottom: 30px;">
        <h1 style="text-align: center">ユーザー編集</h1>

        <h5 style="text-align: center; color: red; padding-top: 15px">
            ${errMsg }
        </h5>

        <form action="/MyWebSite/UserEdit" method="post">
            <div align="center" style="padding-top: 30px">
                <table>
                    <tr>
                        <td class="info">パスワード</td>
                        <td class="info"><input name="password" type="password"></td>
                    </tr>
                    <tr>
                        <td class="info">パスワード(確認)</td>
                        <td class="info"><input name="confirm" type="password"></td>
                    </tr>
                    <tr>
                        <td class="info">ユーザー名</td>
                        <td class="info"><input name="name" type="text" value="${name }"></td>
                    </tr>
                    <tr>
                        <td class="info">生年月日</td>
                        <td class="info"><input name="birthDate" type="date" value="${birthDate }"></td>
                    </tr>
                </table>

                <input type="submit" value="更新" style="width: 100px; margin-top: 30px;">
            </div>
        </form>

        <a href="javascript:history.back()" style="text-decoration: underline">戻る</a>

    </div>

</body>

</html>