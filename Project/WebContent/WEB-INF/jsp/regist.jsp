<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">

    <title></title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style1.css">

</head>

<body>
    <div class="container header">
        <div class="row">
            <h1 class="col-3" style="margin-left: 20px">
                <a class="home" href="/MyWebSite/Login" style="text-decoration: none">MyWebSite</a>
            </h1>

        </div>
    </div>

    <div class="container body col-5" style="padding-top: 30px;padding-bottom: 30px;">
        <h1 style="text-align: center">ユーザー新規登録</h1>

         <div style="text-align: center; margin-top: 10px">
            <span style="color: red">${errMsg }</span>
        </div>

        <form action="/MyWebSite/Regist" method="post">
            <div align="center" style="padding-top: 30px">
                <table>
                    <tr>
                        <td class="info" style="padding-right: 50px">ログインID</td>
                        <td class="info"><input name="loginId" type="text" value="${loginId }"></td>
                    </tr>
                    <tr>
                        <td class="info">ユーザー名</td>
                        <td class="info"><input name="name" type="text" value="${name }"></td>
                    </tr>
                    <tr>
                        <td class="info">パスワード</td>
                        <td class="info"><input name="password" type="password"></td>
                    </tr>
                    <tr>
                        <td class="info">パスワード(確認)</td>
                        <td class="info"><input name="confirm" type="password"></td>
                    </tr>
                    <tr>
                        <td class="info">生年月日</td>
                        <td class="info"><input name="birthDate" type="date" value="${birthDate }" style="width: 172px"></td>
                    </tr>
                </table>

                <input type="submit" value="登録" style="width: 100px; margin-top: 30px;">
            </div>

            <a href="javascript:history.back()" style="text-decoration: underline">戻る</a>

        </form>

    </div>

</body>

</html>