<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">

    <title></title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

     <link rel="stylesheet" href="css/style1.css">

</head>

<body>
    <div class="container header">
        <div class="row">
            <h1 class="col-3" style="margin-left: 20px">
                <a class="home" href="/MyWebSite/Login" style="text-decoration: none">MyWebSite</a>
            </h1>
            <div class="col" style="align-self: flex-end; text-align: right">
                <span>${userInfo.name }さん</span>
                <a class="mypage" href="/MyWebSite/UserDetail">マイページ</a>
                <a class="logout" href="/MyWebSite/Logout">ログアウト</a>
            </div>

        </div>
    </div>

    <div class="container body col-5" style="padding-top: 30px;padding-bottom: 30px;">
        <h1 style="text-align: center">ユーザー情報</h1>

        <div align="center" style="padding-top: 30px">

            <div>
                <a href="/MyWebSite/UserEdit">編集</a>
            </div>

            <table style="margin-top: 10px">
                <tr>
                    <td class="info" style="padding-right: 50px">ログインID</td>
                    <td class="info">${user.loginId }</td>
                </tr>
                <tr>
                    <td class="info">ユーザー名</td>
                    <td class="info">${user.name }</td>
                </tr>
                <tr>
                    <td class="info">生年月日</td>
                    <td class="info">${user.birthDate }</td>
                </tr>
                <tr>
                    <td class="info">登録日時</td>
                    <td class="info">${user.createDate }</td>
                </tr>
                <tr>
                    <td class="info">更新日時</td>
                    <td class="info">${user.updateDate }</td>
                </tr>
            </table>

            <div style="padding-top: 10px">
                <a href="/MyWebSite/UserDelete?id=${user.id }" style="color: red; text-decoration: underline">ユーザー削除</a>
            </div>

            <div style="padding-top: 30px;">
                <a href="/MyWebSite/Login" style="text-decoration: underline">ホームへ</a>
            </div>
        </div>

		<a href="javascript:history.back()" style="text-decoration: underline">戻る</a>

    </div>

</body>

</html>