<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">

    <title></title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style1.css">

</head>

<body>
    <div class="container header">
        <div class="row">
            <h1 class="col-3" style="margin-left: 20px">
                <a class="home" href="" style="text-decoration: none">MyWebSite</a>
            </h1>

            <div class="col" style="align-self: flex-end; text-align: right">
                <span>高橋さん</span>
                <a class="mypage" href="">マイページ</a>
                <a class="logout" href="">ログアウト</a>
            </div>

        </div>
    </div>

    <div class="container body col-5" style="padding-top: 50px;padding-bottom: 50px;">
        <div style="text-align: center">
            <div>このコメントを削除します
            </div>

            <div style="padding-top: 30px;">
                <a href="" style="text-decoration: underline;padding-right: 50px">キャンセル</a>
                <a href="" style="text-decoration: underline">削除</a>
            </div>
        </div>
    </div>

</body>

</html>