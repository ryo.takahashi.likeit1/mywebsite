<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">

    <title></title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

     <link rel="stylesheet" href="css/style1.css">

</head>

<body>
    <div class="container header">
        <div class="row">
            <h1 class="col-3" style="margin-left: 20px">
                <a class="home" href="/MyWebSite/Login" style="text-decoration: none">MyWebSite</a>
            </h1>
            <div class="col" style="align-self: flex-end; text-align: right">
                <span>${userInfo.name }さん</span>
                <a class="mypage" href="/MyWebSite/UserDetail">マイページ</a>
                <a class="logout" href="/MyWebSite/Logout">ログアウト</a>
            </div>
        </div>
    </div>

    <div class="container body" style="padding-top: 30px;padding-bottom: 30px; margin-bottom: 30px">
        <h4><span class="bbstitle">${bbs.title }</span></h4>

        <div class="container">
            <div>
                <br>名前：
                <a class="name" href="">${cm.userName }</a>
                <br>${cm.createDate }
                <div class="text" >${cm.text }</div>
            </div>

        </div>

        <div class="container" align="center" style="padding-top: 30px;">
            <h5 style="color: red">※このコメントを削除してよろしいでしょうか？</h5>
            <div style="display: inline-flex;margin-top: 15px">
                <form action="/MyWebSite/ReportDelete" method="post">
              		<input type="hidden" name="reportId" value="${report.id }">
                    <input type="submit" value="キャンセル" style="margin-right: 30px">
                </form>
                <form action="/MyWebSite/ReportDetail" method="post">
                    <input type="hidden" name="reportId" value="${report.id }">
                    <input type="hidden" name="cmId" value="${cm.id }">
                    <input type="submit" value="削除" style="width: 96px">
                </form>
            </div>
        </div>
    </div>

</body>

</html>