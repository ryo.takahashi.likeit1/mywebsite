<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">

    <title></title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style1.css">

</head>

<body>
    <div class="container header">
        <div class="row">
            <h1 class="col-3" style="margin-left: 20px">
                <a class="home" href="/MyWebSite/Login" style="text-decoration: none">MyWebSite</a>
            </h1>


        </div>
    </div>

    <div class="container body col-5" style="padding-top: 30px;padding-bottom: 30px;">
        <div style="text-align: center">
            <h3 style="margin-bottom: 30px">ユーザーを削除しました</h3>
            <a href="/MyWebSite/Login" style="text-decoration: underline">ログイン画面へ</a>
        </div>
    </div>

</body>

</html>