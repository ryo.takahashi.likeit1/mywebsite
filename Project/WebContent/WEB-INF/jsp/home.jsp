<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">

    <title></title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style1.css">

</head>

<body>
    <div class="container header">
        <div class="row">
            <h1 class="col-3" style="margin-left: 20px">
                <a class="home" href="/MyWebSite/Login" style="text-decoration: none">MyWebSite</a>
            </h1>
            <div class="col" style="align-self: flex-end; text-align: right">
                <span>${userInfo.name }さん</span>
                <a class="mypage" href="/MyWebSite/UserDetail">マイページ</a>
                <a class="logout" href="/MyWebSite/Logout">ログアウト</a>
            </div>
        </div>
    </div>

    <div class="container body col-5" style="padding-top: 40px;padding-bottom: 30px;">

        <h5 style="text-align: center">

            <div style="padding-top: 20px">
                メニュー
            </div>
        </h5>

        <div style="display: flex; align-items: center;flex-direction: column">
            <ol>
                <li><a href="/MyWebSite/BBSList">掲示板一覧</a></li>
                <li><a href="/MyWebSite/UserList">ユーザー一覧</a></li>
                <li><a href="/MyWebSite/UserDetail">マイページ</a></li>
                <c:if test="${userInfo.loginId == 'admin' }">
                <li><a href="/MyWebSite/ReportList">報告コメント一覧</a></li>
                </c:if>
            </ol>
        </div>

    </div>

</body>

</html>