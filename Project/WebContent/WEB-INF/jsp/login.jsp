<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">

    <title></title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style1.css">

</head>

<body>
    <div class="container header">
        <div class="row">
            <h1 class="col-3" style="margin-left: 20px">
                <a class="home" href="" style="text-decoration: none">MyWebSite</a>
            </h1>
        </div>
    </div>

    <div class="container body col-5" style="padding-top: 30px;padding-bottom: 30px;">
        <h1 style="text-align: center">ログイン</h1>

        <div style="text-align: center; padding-top: 10px">
            <span style="color: red;">${errMsg}</span>
        </div>

        <h5 style="text-align: center; color: red;padding-top:10px"></h5>

        <div align="center" style="padding-top: 10px;">

        <form action="/MyWebSite/Login" method="post">
        	<table>
            	<tr>
                	<td style="padding-right: 50px; padding-bottom: 15px">ログインID</td>
                	<td style="padding-bottom: 15px"><input name="loginId" type="text"></td>
            	</tr>
            	<tr>
                	<td>パスワード</td>
                	<td><input name="password" type="password"></td>
            	</tr>
        	</table>

        	<input type="submit" value="ログイン" style="margin-top: 30px">
        </form>

        </div>

        <div align="center" style="padding-top: 30px">
            <a href="/MyWebSite/Regist" style="text-decoration: underline">新規登録</a>
        </div>

    </div>

</body>

</html>