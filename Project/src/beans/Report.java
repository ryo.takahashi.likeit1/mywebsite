package beans;

import java.io.Serializable;

public class Report implements Serializable {

	private int id;
	private String bbsId;
	private String bbsTitle;
	private String commentId;
	private String comment;
	private String reportDate;

	// すべてのデータをセットするコンストラクタ
	public Report(int id, String bbsId, String bbsTitle, String commentId, String comment, String reportDate) {

		this.id = id;
		this.bbsId = bbsId;
		this.bbsTitle = bbsTitle;
		this.commentId = commentId;
		this.comment = comment;
		this.reportDate = reportDate;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBbsId() {
		return bbsId;
	}

	public void setBbsId(String bbsId) {
		this.bbsId = bbsId;
	}

	public String getCommentId() {
		return commentId;
	}

	public void setCommentId(String commentId) {
		this.commentId = commentId;
	}

	public String getReportDate() {
		return reportDate;
	}

	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

	public String getBbsTitle() {
		return bbsTitle;
	}

	public void setBbsTitle(String bbsTitle) {
		this.bbsTitle = bbsTitle;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
