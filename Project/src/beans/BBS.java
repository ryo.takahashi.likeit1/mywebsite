package beans;

import java.io.Serializable;

public class BBS implements Serializable {

	private int id;
	private String title;
	private String userId;
	private String categoryId;
	private String text;
	private String createDate;

	// すべてのデータをセットするコンストラクタ
	public BBS(int id, String title, String userId, String categoryId, String text, String createDate) {

		this.id = id;
		this.title = title;
		this.userId = userId;
		this.categoryId = categoryId;
		this.text = text;
		this.createDate = createDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

}
