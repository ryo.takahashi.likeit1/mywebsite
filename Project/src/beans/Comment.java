package beans;

import java.io.Serializable;

public class Comment implements Serializable {

	private int id;
	private String bbsId;
	private String userId;
	private String userName;
	private String text;
	private String createDate;

	// すべてのデータをセットするコンストラクタ
	public Comment(int id, String bbsId, String userId,String userName, String text, String createDate) {

		this.id = id;
		this.bbsId = bbsId;
		this.userId = userId;
		this.userName = userName;
		this.text = text;
		this.createDate = createDate;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBbsId() {
		return bbsId;
	}

	public void setBbsId(String bbsId) {
		this.bbsId = bbsId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
