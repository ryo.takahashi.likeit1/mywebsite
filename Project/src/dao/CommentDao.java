package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;

public class CommentDao {

	/** コメント登録を行う **/

	public int createComment(String bbsId, int userId, String name, String text) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();


			String sql = "INSERT INTO comment(bbs_id, user_id, user_name, text, create_date)"
					+ "VALUES (?,?,?,?,now())";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, bbsId);
			pStmt.setInt(2, userId);
			pStmt.setString(3, name);
			pStmt.setString(4, text);

			int result = pStmt.executeUpdate();

			return result;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}

	/** 掲示板IDによるコメント検索 **/

	public List<Comment> findByBBSId(String bbsId) {
		Connection conn = null;
		List<Comment> cmList = new ArrayList<Comment>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM comment WHERE bbs_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, bbsId);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {

				int id = rs.getInt("id");

				/** textの改行コードを変換 **/
				String sql2 = "SELECT REPLACE(text,'\r\n','<br />') FROM comment WHERE id = ?";
				PreparedStatement pStmt2 = conn.prepareStatement(sql2);
				pStmt2.setInt(1, id);
				ResultSet rs2 = pStmt2.executeQuery();

				if (!rs2.next()) {
					return null;
				}

				String text = rs2.getString(1);

				String bbs_id = rs.getString("bbs_id");
				String userId = rs.getString("user_id");
				String userName = rs.getString("user_name");
				String createDate = rs.getString("create_date");
				Comment cm =new Comment(id, bbs_id, userId, userName, text, createDate);

				cmList.add(cm);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return cmList;
	}

	/** IDによるコメント検索 **/

	public Comment findId(String id) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM comment WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}


			int set_id = rs.getInt("id");
			String bbs_id = rs.getString("bbs_id");
			String userId = rs.getString("user_id");
			String userName = rs.getString("user_name");
			String createDate = rs.getString("create_date");

			/** textの改行コードを変換 **/
			String sql2 = "SELECT REPLACE(text,'\r\n','<br />') FROM comment WHERE id = ?";
			PreparedStatement pStmt2 = conn.prepareStatement(sql2);
			pStmt2.setString(1, id);
			ResultSet rs2 = pStmt2.executeQuery();

			if (!rs2.next()) {
				return null;
			}

			String text = rs2.getString(1);

			Comment cm =new Comment(set_id, bbs_id, userId, userName, text, createDate);

			return cm;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/** コメントを削除されたコメントに書き換え **/

	public void deleteComment(String commentId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// INSERT文を準備
			String sql = "UPDATE comment SET text = '<br><span style=\"color: red\">※このコメントは削除されました</span>' WHERE id = ?";

			// SQLを実行しユーザーを登録
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, commentId);
			pStmt.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
