package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import beans.Category;

public class CategoryDao {

	/** IDによるカテゴリー検索 **/

	public Category findName(String id) {

		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SQL文を準備
			String sql = "SELECT * FROM category WHERE id = ?";

			// SQLを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 取得したデータをBeansに追加
			int setId = rs.getInt("id");
			String name = rs.getString("name");

			Category category = new Category(setId, name);

			return category;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

	/** カテゴリー名による検索 **/

	public Category findId(String categoryName) {

		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SQL文を準備
			String sql = "SELECT * FROM category WHERE name = ?";

			// SQLを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, categoryName);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 取得したデータをBeansに追加
			int setId = rs.getInt("id");
			String name = rs.getString("name");

			Category category = new Category(setId, name);

			return category;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}
}
