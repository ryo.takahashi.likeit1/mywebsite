package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.Report;

public class ReportDao {

	/** 報告コメント登録を行う **/

	public void createReport(String bbsId, String bbsTitle, String commentId, String comment) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();


			String sql = "INSERT INTO report(bbs_id, bbs_title, comment_id, comment, report_date)"
					+ "VALUES (?,?,?,?,now())";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, bbsId);
			pStmt.setString(2, bbsTitle);
			pStmt.setString(3, commentId);
			pStmt.setString(4, comment);

			pStmt.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/** すべての報告データを報告された日付が新しい順に取得 **/

	public List<Report> findAll() {
		Connection conn = null;
		List<Report> reportList = new ArrayList<Report>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM report ORDER BY report_date DESC";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String bbsId = rs.getString("bbs_id");
				String bbsTitle = rs.getString("bbs_title");
				String commentId = rs.getString("comment_id");
				String comment = rs.getString("comment");
				String reportDate = rs.getString("report_date");
				Report report = new Report(id, bbsId, bbsTitle, commentId, comment, reportDate);

				reportList.add(report);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return reportList;
	}

	/** IDによる検索 **/

	public Report findById(String id) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql ="SELECT * FROM report WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			/** commentの改行コードを変換 **/
			String sql2 = "SELECT REPLACE(comment,'\r\n','<br />') FROM report WHERE id = ?";
			PreparedStatement pStmt2 = conn.prepareStatement(sql2);
			pStmt2.setString(1, id);
			ResultSet rs2 = pStmt2.executeQuery();

			if (!rs2.next()) {
				return null;
			}

			String comment = rs2.getString(1);

			int setId = rs.getInt("id");
			String bbsId = rs.getString("bbs_id");
			String bbsTitle = rs.getString("bbs_title");
			String commentId = rs.getString("comment_id");
			String reportDate = rs.getString("report_date");
 			Report report = new Report(setId, bbsId, bbsTitle, commentId, comment, reportDate);

 			return report;

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/** 報告情報を削除 **/

	public void reportDelete(String reportId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// INSERT文を準備
			String sql = "DELETE FROM report WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, reportId);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
