package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.BBS;

public class BBSDao {

	/** 掲示板登録を行う **/

	public int createBBS(String title, String loginId, String category, String text) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			// カテゴリーIDを取得
			String sql = "SELECT * FROM category WHERE name = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, category);
			ResultSet rs = pStmt .executeQuery();

			if (!rs.next()) {
				return 0;
			}
			category = rs.getString("id");

			sql = "INSERT INTO bbs(title, user_id, category_id, text, create_date)"
					+ "VALUES (?,?,?,?,now())";

			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, title);
			pStmt.setString(2, loginId);
			pStmt.setString(3, category);
			pStmt.setString(4, text);

			int result = pStmt.executeUpdate();

			return result;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}

	/** すべての掲示板データを更新日が新しい順に取得 **/

	public List<BBS> findAll() {
		Connection conn = null;
		List<BBS> bbsList = new ArrayList<BBS>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * \r\n" +
					"FROM bbs t \r\n" +
					"LEFT OUTER JOIN \r\n" +
					"(SELECT c.bbs_id,MAX(c.create_date) n_date FROM comment c GROUP BY c.bbs_id) c\r\n" +
					"ON t.id = c.bbs_id  \r\n" +
					"ORDER BY c.n_date DESC";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String title = rs.getString("title");
				String userId = rs.getString("user_id");
				String categoryId = rs.getString("category_id");
				String text = rs.getString("text");
				String createDate = rs.getString("create_date");
				BBS bbs =new BBS(id, title, userId, categoryId, text, createDate);

				bbsList.add(bbs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return bbsList;
	}

	/** 掲示板IDによる掲示板検索 **/

	public BBS findBBS(String id) {

		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SQL文を準備
			String sql = "SELECT * FROM bbs WHERE id = ?";

			// SQLを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			int bbsId = rs.getInt("id");
			String title = rs.getString("title");
			String userId = rs.getString("user_id");
			String categoryId = rs.getString("category_id");
			String createDate = rs.getString("create_date");

			/** textの改行コードを変換 **/
			String sql2 = "SELECT REPLACE(text,'\r\n','<br />') FROM BBS WHERE id = ?";
			PreparedStatement pStmt2 = conn.prepareStatement(sql2);
			pStmt2.setString(1, id);
			ResultSet rs2 = pStmt2.executeQuery();

			if (!rs2.next()) {
				return null;
			}

			String text = rs2.getString(1);

			// 取得したデータをBeansに追加
			BBS bbs = new BBS(bbsId, title, userId, categoryId, text, createDate);

			return bbs;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

	/** 掲示板名の部分一致検索 **/

	public List<BBS> findByTitle(String title) {
		Connection conn = null;
		List<BBS> bbsList = new ArrayList<BBS>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * \r\n" +
					"FROM bbs t \r\n" +
					"LEFT OUTER JOIN \r\n" +
					"(SELECT c.bbs_id,MAX(c.create_date) n_date FROM comment c GROUP BY c.bbs_id) c\r\n" +
					"ON t.id = c.bbs_id \r\n" +
					"WHERE title LIKE ?	\r\n" +
					"ORDER BY c.n_date DESC;";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, "%" + title + "%");
			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String bbstitle = rs.getString("title");
				String userId = rs.getString("user_id");
				String categoryId = rs.getString("category_id");
				String text = rs.getString("text");
				String createDate = rs.getString("create_date");
				BBS bbs =new BBS(id, bbstitle, userId, categoryId, text, createDate);

				bbsList.add(bbs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return bbsList;
	}

	/** カテゴリーでの掲示板検索 **/

	public List<BBS> findByCategory(int categoryId) {
		Connection conn = null;
		List<BBS> bbsList = new ArrayList<BBS>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * \r\n" +
					"FROM bbs t \r\n" +
					"LEFT OUTER JOIN \r\n" +
					"(SELECT c.bbs_id,MAX(c.create_date) n_date FROM comment c GROUP BY c.bbs_id) c\r\n" +
					"ON t.id = c.bbs_id \r\n" +
					"WHERE category_id = ?\r\n" +
					"ORDER BY c.n_date DESC;";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, categoryId);
			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String bbstitle = rs.getString("title");
				String userId = rs.getString("user_id");
				String setCategoryId = rs.getString("category_id");
				String text = rs.getString("text");
				String createDate = rs.getString("create_date");
				BBS bbs =new BBS(id, bbstitle, userId, setCategoryId, text, createDate);

				bbsList.add(bbs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return bbsList;
	}

	/** 掲示板のタイトル部分一致とカテゴリーから合わせて検索 **/

	public List<BBS> findByTitleAndCategory(String title, int categoryId) {
		Connection conn = null;
		List<BBS> bbsList = new ArrayList<BBS>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * \r\n" +
					"FROM bbs t \r\n" +
					"LEFT OUTER JOIN \r\n" +
					"(SELECT c.bbs_id,MAX(c.create_date) n_date FROM comment c GROUP BY c.bbs_id) c\r\n" +
					"ON t.id = c.bbs_id \r\n" +
					"WHERE title LIKE ? \r\n" +
					"AND category_id = ? \r\n" +
					"ORDER BY c.n_date DESC;";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, "%" + title + "%");
			pStmt.setInt(2, categoryId);
			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String bbstitle = rs.getString("title");
				String userId = rs.getString("user_id");
				String setCategoryId = rs.getString("category_id");
				String text = rs.getString("text");
				String createDate = rs.getString("create_date");
				BBS bbs =new BBS(id, bbstitle, userId, setCategoryId, text, createDate);

				bbsList.add(bbs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return bbsList;
	}

	/** コメントIDによる検索 **/

	public BBS findByCommentId(String id) {

		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SQL文を準備
			String sql = "SELECT t1.id, t1.title, t1.user_id, t1.category_id, t1.text, t1.create_date\r\n" +
						  "FROM bbs t1 INNER JOIN comment t2 ON t1.id = t2.bbs_id WHERE t2.id = ?";

			// SQLを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			int bbsId = rs.getInt("id");
			String title = rs.getString("title");
			String userId = rs.getString("user_id");
			String categoryId = rs.getString("category_id");
			String text = rs.getString("text");
			String createDate = rs.getString("create_date");

			/** textの改行コードを変換 **/
			String sql2 = "SELECT REPLACE(t1.text,'\\r\\n','<br />') "
					+ "FROM BBS t1 INNER JOIN comment t2 ON t1.id = t2.bbs_id WHERE t2.id = ?";
			PreparedStatement pStmt2 = conn.prepareStatement(sql2);
			pStmt2.setString(1, id);
			ResultSet rs2 = pStmt2.executeQuery();

			if (!rs2.next()) {
				return null;
			}

			text = rs2.getString(1);


			// 取得したデータをBeansに追加
			BBS bbs = new BBS(bbsId, title, userId, categoryId, text, createDate);

			return bbs;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}
}
