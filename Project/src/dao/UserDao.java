package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import beans.User;

public class UserDao {

	/** ログインIDとパスワードに紐づくユーザ情報を返す **/

	public User findByLoginInfo(String loginId, String password) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, loginId);
			stmt.setString(2, password);
			ResultSet rs = stmt .executeQuery();

			// ユーザー情報が存在しない場合nullを返す
			if (!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			User user = new User(id,loginIdData, nameData);
			return user;


		}catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/** 渡された値を使用しユーザー登録を行う **/

	public int userRegist(String loginId, String password, String name, String birthDate) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// INSERT文を準備
			String sql = "INSERT INTO user (login_id,name,birth_date,password,create_date,update_date)"
					+ "VALUES (?,?,?,?,now(),now())";

			// SQLを実行しユーザーを登録
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, name);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, password);
			int result = pStmt.executeUpdate();

			return result;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}

	}

	/** ログインIDに紐づくユーザー情報の検索 **/

	public User findByLoginId(String loginId) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ?";

			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, loginId);
			ResultSet rs = stmt .executeQuery();

			// ユーザー情報が存在しない場合nullを返す
			if (!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			String password = rs.getString("password");
			Date birthDate = rs.getDate("birth_date");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			User user = new User(id,loginIdData, nameData, password, birthDate, createDate, updateDate);
			return user;


		}catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/** ユーザー情報の更新 **/

	public int update(String loginId, String name, String birthDate, String password) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// INSERT文を準備
			String sql = "UPDATE user SET name = ?,birth_date = ?,password = ?, update_date = now() "
					+ "WHERE login_id = ?";

			// SQLを実行しユーザーを登録
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(4, loginId);
			pStmt.setString(1, name);
			pStmt.setString(2, birthDate);
			pStmt.setString(3, password);
			int result = pStmt.executeUpdate();

			return result;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}

	/** パスワードの項目以外のユーザー情報の更新 **/

	public int updateExceptPassword(String loginId, String name, String birthDate) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// INSERT文を準備
			String sql = "UPDATE user SET name = ?,birth_date = ?, update_date = now() WHERE login_id = ?";

			// SQLを実行しユーザーを登録
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(3, loginId);
			pStmt.setString(1, name);
			pStmt.setString(2, birthDate);
			int result = pStmt.executeUpdate();

			return result;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}

	/**管理者以外の全てのユーザ情報を取得する*/

	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user where id not in(1)";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, password, birthDate, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	/** IDを元にユーザー情報を検索 **/
	public User findUser(String id) {

		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SQL文を準備
			String sql = "SELECT * FROM user WHERE id = ?";

			// SQLを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 取得したデータをBeansに追加
			int setid = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			User user = new User(setid, loginId, name, password, birthDate, createDate, updateDate);

			return user;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

	/** ユーザーを削除 **/
	public void userDelete(String id) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// INSERT文を準備
			String sql = "DELETE FROM user WHERE id = ?";

			// SQLを実行しユーザーを登録
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/** ユーザー名をもとにnameを部分一致検索 **/
	public List<User> findByName(String searchName) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE name LIKE ? AND  id not in(1)";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, "%" + searchName + "%");
			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, password,birthDate, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}
}
