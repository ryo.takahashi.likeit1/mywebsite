package ws;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;

/**
 * Servlet implementation class UserDelete
 */
@WebServlet("/UserDelete")
public class UserDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** セッションにログイン情報がない場合、ログイン画面に遷移**/
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}
		/** 文字コードを指定 **/
		request.setCharacterEncoding("UTF-8");

		/** IDを取得 **/
		String id = request.getParameter("id");

		/** IDによるユーザー検索 **/
		UserDao userdao = new UserDao();
		User user = userdao.findUser(id);

		/** 検索結果のユーザー情報をリクエストスコープにセット **/
		request.setAttribute("user", user);

		/** ユーザー削除画面にフォワード **/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdelete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** 文字コードを指定 **/
		request.setCharacterEncoding("UTF-8");

		/** IDを取得 **/
		String id = request.getParameter("id");

		/** ユーザーを削除 **/
		UserDao userdao = new UserDao();
		userdao.userDelete(id);

		/** ログイン時にセッションに保存したユーザー情報を破棄 **/
		HttpSession session = request.getSession();
		session.removeAttribute("userInfo");

		/** 削除確認画面にフォワード **/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdeletecomp.jsp");
		dispatcher.forward(request, response);
	}

}
