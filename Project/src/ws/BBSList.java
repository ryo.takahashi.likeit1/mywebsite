package ws;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BBS;
import beans.Category;
import dao.BBSDao;
import dao.CategoryDao;

/**
 * Servlet implementation class BBSList
 */
@WebServlet("/BBSList")
public class BBSList extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BBSList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** セッションにログイン情報がない場合、ログイン画面に遷移**/
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** すべての掲示板情報を更新日が新しい順に取得 **/
		BBSDao bbsdao = new BBSDao();
		List<BBS> bbsList = bbsdao.findAll();

		/** 掲示板リスト情報をスコープにセット **/
		request.setAttribute("bbsList", bbsList);

		/** 掲示板一覧画面にフォワード **/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/bbslist.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** 文字コードを指定 **/
		request.setCharacterEncoding("UTF-8");

		/** セッションにログイン情報がない場合、ログイン画面に遷移**/
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** リクエストスコープから検索情報を取得 **/
		String title = request.getParameter("title");
		String categoryName = request.getParameter("category");

		System.out.println(title);
		System.out.println(categoryName);

		/** 検索用にDAOのインスタンスを作成 **/
		BBSDao bbsdao = new BBSDao();
		CategoryDao ctDao = new CategoryDao();

		/** タイトル名または選択されたカテゴリーで掲示板検索 **/
		if(title.equals("")) {
			// タイトルが未入力かつカテゴリーも選択されていない場合、エラーメッセージをセット
			if(categoryName.equals("")) {
				request.setAttribute("errMsg", "タイトルまたはカテゴリーを選択してください");
				List<BBS> bbsList = bbsdao.findAll();
				request.setAttribute("bbsList", bbsList);
				System.out.println("1");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/bbslist.jsp");
				dispatcher.forward(request, response);
			// タイトルが未入力でカテゴリーが選択されている場合、カテゴリーでの掲示板検索を行う
			} else {
				Category ct = ctDao.findId(categoryName);
				int categoryId = ct.getId();
				List<BBS> bbsList = bbsdao.findByCategory(categoryId);
				request.setAttribute("bbsList", bbsList);
				System.out.println("2");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/bbslist.jsp");
				dispatcher.forward(request, response);
			}
		} else {
			// タイトルが入力されていてカテゴリーが選択されていない場合、タイトルの部分一致検索を行う
			if(categoryName.equals("")) {
				List<BBS> bbsList = bbsdao.findByTitle(title);
				request.setAttribute("bbsList", bbsList);
				request.setAttribute("title", title);
				System.out.println("3");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/bbslist.jsp");
				dispatcher.forward(request, response);
				// タイトルとカテゴリー両方が選択されている場合、タイトルとカテゴリーによる検索を行う
			} else {
				Category ct = ctDao.findId(categoryName);
				int categoryId = ct.getId();
				List<BBS> bbsList = bbsdao.findByTitleAndCategory(title, categoryId);
				request.setAttribute("bbsList", bbsList);
				request.setAttribute("title", title);
				System.out.println("4");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/bbslist.jsp");
				dispatcher.forward(request, response);
			}
		}
	}

}
