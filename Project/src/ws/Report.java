package ws;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Comment;
import dao.BBSDao;
import dao.CommentDao;
import dao.ReportDao;

/**
 * Servlet implementation class Report
 */
@WebServlet("/Report")
public class Report extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Report() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** セッションにログイン情報がない場合、ログイン画面に遷移**/
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** 文字コードを指定 **/
		request.setCharacterEncoding("UTF-8");

		/** コメントIDを取得しDAOにて検索 **/
		String id = request.getParameter("id");
		CommentDao cmDao = new CommentDao();
		Comment cm = cmDao.findId(id);

		/** コメントIDによる掲示板検索 **/
		BBSDao bbsdao = new BBSDao();
		beans.BBS bbs = bbsdao.findByCommentId(id);

		/** リクエストスコープにセット **/
		request.setAttribute("cm", cm);
		request.setAttribute("bbs", bbs);

		/** 報告確認画面へ遷移 **/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/reportcomfirm.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** セッションにログイン情報がない場合、ログイン画面に遷移**/
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** 文字コードを指定 **/
		request.setCharacterEncoding("UTF-8");

		/** セットされた値を取得 **/
		String bbsId = request.getParameter("bbsId");
		String cmId = request.getParameter("cmId");

		/** 掲示板とコメントの検索 **/
		BBSDao bbsdao = new BBSDao();
		beans.BBS bbs = bbsdao.findBBS(bbsId);
		String bbsTitle = bbs.getTitle();

		CommentDao cmDao = new CommentDao();
		Comment cm = cmDao.findId(cmId);
		String cmText = cm.getText();

		/** 報告テーブルへの登録を行う **/
		ReportDao rpdao = new ReportDao();
		rpdao.createReport(bbsId, bbsTitle, cmId, cmText);


		/** 掲示板一覧画面へリダイレクト **/
		response.sendRedirect("/MyWebSite/BBSList");

	}

}
