package ws;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Comment;
import dao.BBSDao;
import dao.CommentDao;
import dao.ReportDao;

/**
 * Servlet implementation class ReportDetail
 */
@WebServlet("/ReportDetail")
public class ReportDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReportDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** セッションにログイン情報がない場合、ログイン画面に遷移**/
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** 文字コードを指定 **/
		request.setCharacterEncoding("UTF-8");

		/** スコープからID取得し報告詳細データを検索 **/
		String reportId = request.getParameter("id");
		ReportDao reportDao = new ReportDao();
		beans.Report report = reportDao.findById(reportId);

		/** 報告データから掲示板情報とコメント情報を取得 **/
		BBSDao bbsdao = new BBSDao();
		beans.BBS bbs = bbsdao.findBBS(report.getBbsId());

		CommentDao cmdao = new CommentDao();
		Comment cm = cmdao.findId(report.getCommentId());

		/** スコープにセット **/
		request.setAttribute("report", report);
		request.setAttribute("bbs", bbs);
		request.setAttribute("cm", cm);

		/** 報告詳細に画面遷移 **/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/reportdetail.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** セッションにログイン情報がない場合、ログイン画面に遷移**/
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** 文字コードを指定 **/
		request.setCharacterEncoding("UTF-8");

		/** スコープからデータを取得 **/
		String reportId = request.getParameter("reportId");
		String cmId = request.getParameter("cmId");

		/** コメントを書き換え **/
		CommentDao cmdao = new CommentDao();
		cmdao.deleteComment(cmId);

		/** reportテーブルから削除 **/
		ReportDao reportdao = new ReportDao();
		reportdao.reportDelete(reportId);

		/** 報告一覧にリダイレクト **/
		response.sendRedirect("/MyWebSite/ReportList");
	}
}
