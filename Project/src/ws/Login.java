package ws;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import beans.User;
import dao.UserDao;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** セッションにログイン情報がある場合、ホーム画面にフォワード **/
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") != null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/home.jsp");
			dispatcher.forward(request, response);
		}

		/** login.jspにフォワード **/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** 文字コードを指定 **/
		request.setCharacterEncoding("UTF-8");

		/** 入力されたログインIDとパスワードを取得 **/
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		/** パスワードを暗号化 **/
		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		password = DatatypeConverter.printHexBinary(bytes);

		/** 取得したログインIDとパスワードをもとにユーザーを検索 **/
		UserDao userdao = new UserDao();
		User user = userdao.findByLoginInfo(loginId, password);

		/** ログイン失敗時 **/
		if(user == null) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります");
			// 再びlogin.jspへフォワードし、エラーメッセージを表示
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** ログイン成功時 **/
		// セッションにユーザー情報をセット
		HttpSession session = request.getSession();
		session.setAttribute("userInfo", user);

		/** ホーム画面にリダイレクト **/
		response.sendRedirect("/MyWebSite/Home");
	}

}
