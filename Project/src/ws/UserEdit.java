package ws;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import beans.User;
import dao.UserDao;

/**
 * Servlet implementation class UserEdit
 */
@WebServlet("/UserEdit")
public class UserEdit extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserEdit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** セッションにログイン情報がない場合、ログイン画面に遷移**/
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** ユーザー編集画面にフォワード **/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/useredit.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** セッションにログイン情報がない場合、ログイン画面に遷移**/
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** リクエストパラメータの文字コードを指定 **/
		request.setCharacterEncoding("UTF-8");

		/** セッションスコープからログインIDを取得 **/
		User user1 = (User) session.getAttribute("userInfo");
		String loginId = user1.getLoginId();

		/** idと入力された更新内容を取得 **/
		String password = request.getParameter("password");
		String confirm = request.getParameter("confirm");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		/** パスワードとパスワード(確認)の入力内容が異なる場合
	     またパスワード以外の入力項目に一つでも未入力のものがある場合、入力画面に戻る **/
		if (!password.equals(confirm)) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			// パスワード以外の入力した内容を引き継がせるためリクエストスコープにセット
			request.setAttribute("name", name);
			request.setAttribute("birthDate", birthDate);
			// 入力画面に遷移
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/useredit.jsp");
			dispatcher.forward(request, response);
			return;
		} else if(name.equals("")|| birthDate.equals("")) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			// パスワード以外の入力した内容を引き継がせるためリクエストスコープにセット
			request.setAttribute("name", name);
			request.setAttribute("birthDate", birthDate);
			// 入力画面に遷移
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/useredit.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** パスワードが空欄だった場合パスワード以外の項目を更新
		    パスワードが入力されている場合パスワードも含め更新 **/
		UserDao userdao = new UserDao();
		int result;

		if (password.equals("")) {
			result = userdao.updateExceptPassword(loginId, name, birthDate);
		} else {
			/** パスワードを暗号化 **/
			//ハッシュを生成したい元の文字列
			String source = password;
			//ハッシュ生成前にバイト配列に置き換える際のCharset
			Charset charset = StandardCharsets.UTF_8;
			//ハッシュアルゴリズム
			String algorithm = "MD5";

			//ハッシュ生成処理
			byte[] bytes = null;
			try {
				bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			} catch (NoSuchAlgorithmException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			password = DatatypeConverter.printHexBinary(bytes);

			result = userdao.update(loginId, name, birthDate, password);
		}

		/** 登録に成功した場合、ユーザー詳細画面に戻る(リダイレクト)
	        登録に失敗した場合は入力画面に戻る */
		if (result == 1) {
			response.sendRedirect("/MyWebSite/UserDetail");
		} else {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			// パスワード以外の入力した内容を引き継がせるためリクエストスコープにセット
			request.setAttribute("name", name);
			request.setAttribute("birthDate", birthDate);
			// 入力画面に遷移
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/useredit.jsp");
			dispatcher.forward(request, response);
			return;
		}
	}

}
