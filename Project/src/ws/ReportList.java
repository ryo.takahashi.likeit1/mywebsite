package ws;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Report;
import dao.ReportDao;

/**
 * Servlet implementation class ReportList
 */
@WebServlet("/ReportList")
public class ReportList extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReportList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** セッションにログイン情報がない場合、ログイン画面に遷移**/
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** 文字コードを指定 **/
		request.setCharacterEncoding("UTF-8");

		/** 報告コメントを全て取得しリクエストスコープにセット **/
		ReportDao rpDao = new ReportDao();
		List<Report> reportList = rpDao.findAll();
		request.setAttribute("reportList", reportList);

		/** 報告一覧へフォワード **/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/reportlist.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** セッションにログイン情報がない場合、ログイン画面に遷移**/
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** 文字コードを指定 **/
		request.setCharacterEncoding("UTF-8");

	}

}
