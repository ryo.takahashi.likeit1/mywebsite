package ws;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Category;
import beans.Comment;
import beans.User;
import dao.BBSDao;
import dao.CategoryDao;
import dao.CommentDao;
import dao.UserDao;

/**
 * Servlet implementation class BBS
 */
@WebServlet("/BBS")
public class BBS extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BBS() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** セッションにログイン情報がない場合、ログイン画面に遷移**/
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** 文字コードを指定 **/
		request.setCharacterEncoding("UTF-8");

		/** リクエストスコープから掲示板IDを取得 **/
		String bbsId = request.getParameter("id");

		/** 掲示板IDによる掲示板検索を行う **/
		BBSDao bbsdao = new BBSDao();
		beans.BBS bbs = bbsdao.findBBS(bbsId);

		/** 取得したカテゴリーIDによるカテゴリー名前検索 **/
		CategoryDao ctdao = new CategoryDao();
		Category ct = ctdao.findName(bbs.getCategoryId());

		/** 取得したユーザーIDによるユーザー名検索 **/
		UserDao userdao = new UserDao();
		User user = userdao.findByLoginId(bbs.getUserId());

		/** 取得した掲示板IDによるコメント検索 **/
		CommentDao cmDao = new CommentDao();
		List<Comment> cmList = cmDao.findByBBSId(bbsId);

		/** リクエストスコープに情報をセット **/
		request.setAttribute("bbs", bbs);
		request.setAttribute("ct", ct);
		request.setAttribute("user", user);
		request.setAttribute("cmList", cmList);



		/** 掲示板詳細にフォワード **/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/bbs.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** セッションにログイン情報がない場合、ログイン画面に遷移**/
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** 文字コードを指定 **/
		request.setCharacterEncoding("UTF-8");

		/** セッションスコープからログイン中のユーザー情報を取得 **/
		User loginUser = (User) session.getAttribute("userInfo");
		int userId = loginUser.getId();
		String name = loginUser.getName();

		/** リクエストスコープから情報を取得 **/
		String text = request.getParameter("text");
		String bbsId = request.getParameter("bbsId");

		/** 掲示板IDによる掲示板検索を行う **/
		BBSDao bbsdao = new BBSDao();
		beans.BBS bbs = bbsdao.findBBS(bbsId);

		/** 取得したカテゴリーIDによるカテゴリー名前検索 **/
		CategoryDao ctdao = new CategoryDao();
		Category ct = ctdao.findName(bbs.getCategoryId());

		/** 取得したユーザーIDによるユーザー名検索 **/
		UserDao userdao = new UserDao();
		User user = userdao.findByLoginId(bbs.getUserId());

		/** 取得した掲示板IDによるコメント検索 **/
		CommentDao cmDao = new CommentDao();
		List<Comment> cmList = cmDao.findByBBSId(bbsId);

		/** 本文が未入力の場合、エラーメッセージをセットし表示する **/
		if (text.equals("")) {
			request.setAttribute("errMsg", "本文を入力してください");
			request.setAttribute("user", user);
			request.setAttribute("bbs", bbs);
			request.setAttribute("ct", ct);
			request.setAttribute("cmList", cmList);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/bbs.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** コメントテーブルに登録 **/
		int result = cmDao.createComment(bbsId, userId, name, text);

		/** 登録に失敗した場合、エラーメッセージをセットし表示する **/
		if (result == 0) {
			request.setAttribute("errMsg", "書き込みに失敗しました");
			request.setAttribute("bbs", bbs);
			request.setAttribute("ct", ct);
			request.setAttribute("user", user);
			request.setAttribute("cmList", cmList);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/bbs.jsp");
			dispatcher.forward(request, response);
		}

		/** 掲示板一覧にリダイレクト **/
		response.sendRedirect("/MyWebSite/BBSList");
	}

}
