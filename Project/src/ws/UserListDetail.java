package ws;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;

/**
 * Servlet implementation class UserListDetail
 */
@WebServlet("/UserListDetail")
public class UserListDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** セッションにログイン情報がない場合、ログイン画面に遷移**/
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** 文字コードを指定 **/
		request.setCharacterEncoding("UTF-8");

		/** リクエストスコープからIDを取得 **/
		String id = request.getParameter("id");

		/** ユーザーIDを元にユーザー情報を検索 **/
		UserDao userdao = new UserDao();
		User user2 = userdao.findUser(id);

		/** リクエストスコープにユーザー情報をセット **/
		request.setAttribute("user", user2);


		/** ユーザー一覧詳細にフォワード **/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userListDetail.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
