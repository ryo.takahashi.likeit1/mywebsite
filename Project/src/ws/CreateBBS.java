package ws;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.BBSDao;

/**
 * Servlet implementation class CreateBBS
 */
@WebServlet("/CreateBBS")
public class CreateBBS extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateBBS() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** セッションにログイン情報がない場合、ログイン画面に遷移**/
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** 掲示板作成画面にフォワード **/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/createbbs.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** 文字コードを指定 **/
		request.setCharacterEncoding("UTF-8");

		/** セッションにログイン情報がない場合、ログイン画面に遷移**/
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** セッションスコープからログインIDを取得 **/
		User user1 = (User) session.getAttribute("userInfo");
		String loginId = user1.getLoginId();

		/** 書き込まれた情報を取得 **/
		String title = request.getParameter("title");
		String category = request.getParameter("category");
		String text = request.getParameter("text");

		/** 未記入の項目がある場合、対応したエラーメッセージをセットし表示する **/
		if (title.equals("")){
			request.setAttribute("errMsg", "タイトルを入力してください");
			request.setAttribute("text", text);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/createbbs.jsp");
			dispatcher.forward(request, response);
			return;
		} else if (category.equals("")) {
			request.setAttribute("errMsg", "カテゴリーを選択してください");
			request.setAttribute("title", title);
			request.setAttribute("text", text);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/createbbs.jsp");
			dispatcher.forward(request, response);
			return;
		} else if (text.equals("")) {
			request.setAttribute("errMsg", "本文を入力してください");
			request.setAttribute("title", title);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/createbbs.jsp");
			dispatcher.forward(request, response);
			return;
		}


		/** 掲示板情報をテーブルに登録 **/
		BBSDao bbsdao = new BBSDao();
		int result = bbsdao.createBBS(title, loginId, category, text);

		/** 登録に失敗した場合、エラーメッセージをセットし表示する **/
		if (result == 0) {
			request.setAttribute("errMsg", "登録に失敗しました");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/createbbs.jsp");
			dispatcher.forward(request, response);
		}

		/** 掲示板一覧画面にリダイレクト **/
		response.sendRedirect("/MyWebSite/BBSList");
	}

}
