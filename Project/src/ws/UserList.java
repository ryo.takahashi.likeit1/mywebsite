package ws;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;

/**
 * Servlet implementation class UserList
 */
@WebServlet("/UserList")
public class UserList extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** セッションにログイン情報がない場合、ログイン画面に遷移**/
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** 管理者以外のユーザー全件検索 **/
		UserDao userdao = new UserDao();
		List<User> userlist = userdao.findAll();

		/** ユーザーリスト情報をリクエストスコープにセット **/
		request.setAttribute("userlist", userlist);


		/** ユーザー一覧にフォワード **/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userlist.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** 文字コードを指定 **/
		request.setCharacterEncoding("UTF-8");

		/** 入力された名前を取得 **/
		String name = request.getParameter("name");

		/** 名前を部分一致検索
		 	空白の場合は全件検索を行う**/
		if (name.equals("")) {
			UserDao userdao = new UserDao();
			List<User> userlist = userdao.findAll();
			// ユーザーリスト情報をリクエストスコープにセット
			request.setAttribute("userlist", userlist);
			// ユーザー一覧にフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userlist.jsp");
			dispatcher.forward(request, response);
		} else {
			UserDao userdao = new UserDao();
			List<User> userlist = userdao.findByName(name);

			/** ユーザーリスト情報をリクエストスコープにセット **/
			request.setAttribute("userlist", userlist);


			/** ユーザー一覧にフォワード **/
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userlist.jsp");
			dispatcher.forward(request, response);
		}

	}

}
