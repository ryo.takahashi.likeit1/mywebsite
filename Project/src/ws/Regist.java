package ws;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import dao.UserDao;

/**
 * Servlet implementation class Regist
 */
@WebServlet("/Regist")
public class Regist extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Regist() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** ユーザー登録画面にフォワード **/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/regist.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/** 文字コードを指定 **/
		request.setCharacterEncoding("UTF-8");

		/** 入力された項目を取得 **/
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String confirm = request.getParameter("confirm");
		String birthDate = request.getParameter("birthDate");

		/** パスワードとパスワード(確認)の入力内容が異なる場合
	    	また入力項目に一つでも未入力のものがある場合、入力画面に戻る **/

		if (!password.equals(confirm)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			// パスワード以外の入力した内容を引き継がせるためリクエストスコープにセット
			request.setAttribute("loginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthDate", birthDate);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/regist.jsp");
			dispatcher.forward(request, response);
			return;
		} else if(loginId.equals("")|| password.equals("")|| confirm.equals("")|| name.equals("")|| birthDate.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			// パスワード以外の入力した内容を引き継がせるためリクエストスコープにセット
			request.setAttribute("loginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthDate", birthDate);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/regist.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** パスワードを暗号化 **/
		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		password = DatatypeConverter.printHexBinary(bytes);

		/** 取得した値を引数に渡し、daoのメソッドで登録を行うい戻り値を受け取る **/
		UserDao userdao = new UserDao();
		int result = userdao.userRegist(loginId, password, name, birthDate);
		System.out.println(result);

		/** 登録に成功した場合、ユーザー登録完了画面にフォワード
	    	登録に失敗した場合は入力画面に戻る */

		if (result == 1) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/registcomp.jsp");
			dispatcher.forward(request, response);
		} else {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			// パスワード以外の入力した内容を引き継がせるためリクエストスコープにセット
			request.setAttribute("loginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthDate", birthDate);
			// 入力画面に遷移
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/regist.jsp");
			dispatcher.forward(request, response);
			return;
		}

	}

}
